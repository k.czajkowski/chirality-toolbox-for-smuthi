Chirality toolbox for SMUTHI accompanying "Local versus bulk circular dichroism enhancement by achiral all-dielectric nanoresonators" by
Krzysztof M. Czajkowski and Tomasz J. Antosiewicz. The toolbox requires SMUTHI and joblib modules for Python.

import config
from orientation_averaging import average_chirality_new
from smuthi.particles import FiniteCylinder
from tqdm import trange
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
from smuthi.postprocessing import far_field as ff
from initial_field import PlaneWave
from smuthi.layers import LayerSystem
from smuthi.simulation import Simulation
import sys
import joblib as jl

def sim(wl):
    pw=PlaneWave(wl,0,0,'L')
    ls=LayerSystem(refractive_indices=[nsurr,nsurr])
    particle = FiniteCylinder(cylinder_radius=rad,cylinder_height=he,l_max=l_max,
                              refractive_index=4)
    chirality=average_chirality_new(particle,wl,r,pw,ls,True)
    return chirality

rad0=200
d0=2
V=np.pi*rad0**3*d0
d=float(sys.argv[1])
#disk data
rad=(V/np.pi/d)**(1/3)
he=d*rad
radch=0.
dist=50
r=radch+rad+dist

l_max=3
nsurr=1.33
wlvec=np.linspace(1000,2500,151)
chirality=wlvec*0.
chirality=np.array(jl.Parallel(n_jobs=15)(jl.delayed(sim)(wl) for wl in wlvec))
sio.savemat('orientationres'+str(d)+'.mat',{'wl':wlvec,'chirality':chirality})

import config
from smuthi.particles import FiniteCylinder
from smuthi.layers import LayerSystem
from smuthi.initial_field import PlaneWave
from smuthi.simulation import Simulation
import sys
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
from dipolar_chirality import chirality_x,chirality_z
import joblib as jl
ri=4
nmax=3

rad0=200
d0=2
V=np.pi*rad0**3*d0

#disk data

d=float(sys.argv[1])
rad=(V/np.pi/d)**(1/3)
he=d*rad
radch=0.
dist=50
r=radch+rad+dist

thz = 299792.45800
#layers
nsurr=1.33
nsub=1.33

#chiral
rich=np.sqrt(2+0.1j)
radch=0.0
chi=0.1

#position
dist=50
pch=np.array([[radch+rad+dist,0,0],[0,0,radch+he+dist]])
x,y,z=pch.T
print(x.shape)
    
#lamvec=np.linspace(400,1000,121)
lamvec=np.linspace(1000,2500,151)
#freqvec=np.linspace(80,160,81)
etot_pol=[]
htot_pol=[]
pol='L'
def sim(lam):
    particle = FiniteCylinder(refractive_index=ri,position=[0,0,0],l_max=1,cylinder_radius=rad,cylinder_height=he,n_rank=nmax+5)    
    par=[particle]
    tmat=particle.compute_t_matrix(lam,nsurr)

    dipchiralityx=chirality_x(tmat,lam,x[0],nsurr)
    dipchiralityz=chirality_z(tmat,lam,z[1],nsurr)
    return dipchiralityx,dipchiralityz

dipchirality=np.array(jl.Parallel(n_jobs=10)(jl.delayed(sim)(lam) for lam in lamvec))
dipchiralityx=dipchirality[:,0]
dipchiralityz=dipchirality[:,1]
sio.savemat('dist50chirality_explicitlmax1_xz'+str(d)+'.mat',{'lamvec':lamvec,'dipchiralityx':dipchiralityx,'dipchiralityz':dipchiralityz})

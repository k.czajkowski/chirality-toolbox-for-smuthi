import numpy as np

import matplotlib
font = {'size'   : 22}
matplotlib.rc('font', **font)

import matplotlib.pyplot as plt
import scipy.io as sio

Nwl=151
dvec=np.arange(0.5,2.125,0.125)
chirality=np.zeros((len(dvec),Nwl))
extinction=0*chirality
for ii,d in enumerate(dvec):
    #w=sio.loadmat('chirality_xz_final/dipchirality'+str(d))
    w=sio.loadmat('xz/dist50chirality_explicitlmax1_xz'+str(d))
    chirality[ii,:]=w['dipchiralityx']
    extinction[ii,:]=w['dipchiralityz']

wl=np.linspace(1000,2500,Nwl)    
plt.figure()
extents = lambda f : [f[0] - (f[1] - f[0])/2, f[-1] + (f[1] - f[0])/2]
plt.set_cmap('jet')
plt.imshow(chirality.T, aspect='auto', interpolation='none',
           extent=extents(dvec/2) + extents(wl), origin='lower')
plt.xlabel('Aspect ratio')
plt.ylabel('Wavelength (nm)')
cbar0=plt.colorbar()
cbar0.ax.set_ylabel('Chirality enhancement in x')
plt.clim([0,5])
plt.savefig('chirality_x2.pdf',bbox_inches='tight')

plt.figure()
plt.imshow(extinction.T, aspect='auto', interpolation='none',
           extent=extents(dvec/2) + extents(wl), origin='lower')
plt.xlabel('Aspect ratio')
plt.ylabel('Wavelength (nm)')
cbar=plt.colorbar()
cbar.ax.set_ylabel('Chirality enhancement in z')
plt.clim([0,5])
plt.savefig('chirality_z2.pdf',bbox_inches='tight')

plt.show()

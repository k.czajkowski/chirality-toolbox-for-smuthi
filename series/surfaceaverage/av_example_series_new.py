import config
from surface_averaging import calculate_chirality
from smuthi.particles import FiniteCylinder
from tqdm import trange
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
from smuthi.postprocessing import far_field as ff
from initial_field import PlaneWave
from smuthi.layers import LayerSystem
from smuthi.simulation import Simulation
import sys
import joblib as jl

rad0=200
d0=2
V=np.pi*rad0**3*d0

def sim(iwl):
    wl=wlvec[iwl]
    particle = FiniteCylinder(cylinder_radius=rad,cylinder_height=he,l_max=l_max,n_rank=8,position=[0,0,3*rad],refractive_index=4)
    chirality=calculate_chirality(particle,wl,r,nsurr)
    return chirality

#disk data
d=float(sys.argv[1])
rad=(V/np.pi/d)**(1/3)
he=d*rad
radch=0.
dist=50
r=radch+rad+dist
l_max=3
nsurr=1.33
wlvec=np.linspace(1000,2500,151)
chirality=wlvec*0.
extinction=wlvec*0.
chirality=np.array(jl.Parallel(n_jobs=15)(jl.delayed(sim)(iwl) for iwl in range(len(wlvec))))
    
sio.savemat('res'+str(d)+'.mat',{'wl':wlvec,'chirality':chirality})

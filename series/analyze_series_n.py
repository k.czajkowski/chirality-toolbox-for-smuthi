import config
import numpy as np

import matplotlib
font = {'size'   : 22}
matplotlib.rc('font', **font)

import matplotlib.pyplot as plt
import scipy.io as sio

Nwl=151
dvec=np.linspace(0.5,3,81)
dvec=dvec[dvec<=2]
chirality=np.zeros((len(dvec),Nwl))
extinction=0*chirality
for ii,d in enumerate(dvec):
    w=sio.loadmat('surfaceaverage/res'+str(d))
    chirality[ii,:]=w['chirality']
    extinction[ii,:]=w['extinction']

wl=np.linspace(1000,2500,Nwl)    
plt.figure()
extents = lambda f : [f[0] - (f[1] - f[0])/2, f[-1] + (f[1] - f[0])/2]
plt.set_cmap('jet')
plt.imshow(chirality.T, aspect='auto', interpolation='none',
           extent=extents(dvec/2) + extents(wl), origin='lower')
plt.xlabel('Aspect ratio')
plt.ylabel('Wavelength (nm)')
cbar0=plt.colorbar()
cbar0.ax.set_ylabel('Chirality enhancement')
plt.savefig('nchiralityav.pdf',bbox_inches='tight')

plt.show()

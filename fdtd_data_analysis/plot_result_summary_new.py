from plotconfig import *
import matplotlib
matplotlib.rc('font', **font)
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio


res=sio.loadmat('new_result_summary_new.mat')
d=res['d'][0]
resn=res['res']
resn1=res['res1']
print(resn.shape)
nsub=[1.33,1.5,2.0]
plt.figure(figsize=(width*0.9,height))
for kk in range(2,5):
    for jj in range(3):
        plt.subplot(1,3,jj+1)
        plt.plot(d/2,60*np.pi*resn[:,jj,kk],'-o')
        plt.xlabel('Aspect ratio')
        plt.ylabel('Mean chirality enhancement')
        plt.title('nsub='+str(nsub[jj]))
plt.legend(['20 nm','50 nm', '100 nm'])
plt.tight_layout()
plt.savefig('nfig3.pdf',bbox_inches='tight')

plt.figure(figsize=(width*2/3,height))
for jj in range(3):
    plt.subplot(1,2,1)    
    plt.plot(d/2,60*np.pi*resn1[:,jj,0],'-o')
    plt.xlabel('Aspect ratio')
    plt.ylabel('Maximal chirality enhancement')
    plt.subplot(1,2,2)
    plt.plot(d/2,60*np.pi*resn[:,jj,0],'-o')
    
    plt.xlabel('Aspect ratio')
    plt.ylabel('Mean chirality enhancement')
plt.tight_layout()
plt.legend(['nsub='+str(nsub0) for nsub0 in nsub])
plt.savefig('nfig5.pdf',bbox_inches='tight')
plt.show()
print(res.keys())

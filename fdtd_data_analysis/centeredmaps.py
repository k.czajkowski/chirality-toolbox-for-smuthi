import numpy as np

import matplotlib
font = {'size'   : 22}
matplotlib.rc('font', **font)

import matplotlib.pyplot as plt
import scipy.io as sio

w=sio.loadmat('centeredmaps6.mat')
print(w.keys())
maps=w['resim']
x=w['x'][:,0]*1e9
z=w['z'][:,0]*1e9
print(x)
plt.figure()
extents = lambda f : [f[0] - (f[1] - f[0])/2, f[-1] + (f[1] - f[0])/2]
plt.set_cmap('jet')
plt.imshow(maps, aspect='auto', interpolation='none',
           extent=extents(x) + extents(z), origin='lower')
plt.xlabel('x (nm)')
plt.ylabel('z (nm)')
cbar0=plt.colorbar()
cbar0.ax.set_ylabel('Chirality enhancement')
plt.clim([0,12])
plt.savefig('map2.pdf',bbox_inches='tight')

w=sio.loadmat('centeredmaps2.mat')
print(w.keys())
maps=w['resim']
x=w['x'][:,0]*1e9
z=w['z'][:,0]*1e9
print(x)
plt.figure()
extents = lambda f : [f[0] - (f[1] - f[0])/2, f[-1] + (f[1] - f[0])/2]
plt.set_cmap('jet')
plt.imshow(maps, aspect='auto', interpolation='none',
           extent=extents(x) + extents(z), origin='lower')
plt.xlabel('x (nm)')
plt.ylabel('z (nm)')
cbar0=plt.colorbar()
cbar0.ax.set_ylabel('Chirality enhancement')
plt.clim([0,12])
plt.savefig('map1.pdf',bbox_inches='tight')
plt.show()


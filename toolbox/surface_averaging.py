from smuthi.particles import FiniteCylinder
from smuthi.layers import LayerSystem
from initial_field import PlaneWave
from smuthi.fields import multi_to_single_index
from smuthi.simulation import Simulation
import smuthi.utility.math as sf
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

def calculate_chirality(particle,wl,r,nsurr):    
    ls=LayerSystem(refractive_indices=[nsurr,nsurr])
    pw=PlaneWave(wl,0,0,'L',reference_point=particle.position)
    sim=Simulation(particle_list=[particle],layer_system=ls,initial_field=pw)
    sim.run()
    chirality=average_chirality(particle,wl,r,pw,ls)
    return chirality

def calculate_chirality_int2(particle,wl,r,nsurr):    
    ls=LayerSystem(refractive_indices=[nsurr,nsurr])
    pw=PlaneWave(wl,0,0,'L',reference_point=particle.position)
    sim=Simulation(particle_list=[particle],layer_system=ls,initial_field=pw)
    sim.run()
    chirality2=average_chirality_int2(particle,wl,r,ls)
    chirality1=average_chirality_int(particle,wl,r,pw,ls)
    return chirality2,chirality1

def average_chirality_int2(particle,wl,r,ls):
    nsurr=ls.refractive_indices[0]
    pw=PlaneWave(wl,0,0,'L',reference_point=particle.position)
    theta0=np.linspace(0,np.pi,181)
    phi0=np.linspace(0,np.pi*2,101)
    theta,phi=np.meshgrid(theta0,phi0)
    x=r*np.cos(phi)*np.sin(theta)
    y=r*np.sin(phi)*np.sin(theta)
    z=r*np.cos(theta)+particle.position[2]
    einit=pw.electric_field(x,y,z,ls)
    hinit=pw.magnetic_field(x,y,z,ls)
    escat=particle.scattered_field.electric_field(x,y,z)
    hscat=particle.scattered_field.magnetic_field(x,y,z,wl)
    einit = np.array(einit)
    hinit = np.array(hinit)
    hscat = np.array(hscat)
    escat = np.array(escat)
    e=einit+escat
    h=hinit+hscat
    chirscat=np.trapz(np.trapz(np.sum(np.conj(e)*h,axis=0)*np.sin(theta),theta0),phi0)
    return -chirscat.imag/(4*np.pi)/nsurr

def average_chirality_int(particle,wl,r,pw,ls):
    nsurr=ls.refractive_indices[0]
    theta0=np.linspace(0,np.pi,181)
    phi0=np.linspace(0,np.pi*2,361)
    theta,phi=np.meshgrid(theta0,phi0)
    x=r*np.cos(phi)*np.sin(theta)
    y=r*np.sin(phi)*np.sin(theta)
    z=r*np.cos(theta)+particle.position[2]
    einit=pw.spherical_wave_expansion(particle,ls).electric_field(x,y,z)
    hinit=pw.spherical_wave_expansion(particle,ls).magnetic_field(x,y,z,wl)
    escat=particle.scattered_field.electric_field(x,y,z)
    hscat=particle.scattered_field.magnetic_field(x,y,z,wl)
    einit = np.array(einit)
    hinit = np.array(hinit)
    hscat = np.array(hscat)
    escat = np.array(escat)
    chirscat=np.trapz(np.trapz(np.sum(np.conj(escat)*hscat,axis=0)*np.sin(theta),theta0),phi0)
    chireihs=np.trapz(np.trapz(np.sum(np.conj(einit)*hscat,axis=0)*np.sin(theta),theta0),phi0)
    chireshi=np.trapz(np.trapz(np.sum(np.conj(escat)*hinit,axis=0)*np.sin(theta),theta0),phi0)
    chirint=chireshi+chireihs+chirscat
    return 1-chirint.imag/(4*np.pi)/nsurr

def get_fields(particle,wl,r,ls):
    nsurr=ls.refractive_indices[0]
    pw=PlaneWave(wl,0,0,'L',reference_point=particle.position)
    theta0=np.linspace(0,np.pi,361)
    phi0=np.linspace(0,np.pi*2,181)
    theta,phi=np.meshgrid(theta0,phi0)
    x=r*np.cos(phi)*np.sin(theta)
    y=r*np.sin(phi)*np.sin(theta)
    z=r*np.cos(theta)+particle.position[2]
    einit=pw.electric_field(x,y,z,ls)
    hinit=pw.magnetic_field(x,y,z,ls)
    escat=particle.scattered_field.electric_field(x,y,z)
    hscat=particle.scattered_field.magnetic_field(x,y,z,wl)
    return einit,escat,hinit,hscat

def get_fields_spherical(particle,wl,r,phi,theta,ls):
    pw=PlaneWave(wl,0,0,'L',reference_point=particle.position)
    x=r*np.cos(phi)*np.sin(theta)
    y=r*np.sin(phi)*np.sin(theta)
    z=r*np.cos(theta)+particle.position[2]
    print('cartesian:')
    print(x,y,z-particle.position[2])
    einit=pw.electric_field(x,y,z,ls)
    hinit=pw.magnetic_field(x,y,z,ls)
    escat=particle.scattered_field.electric_field(x,y,z)
    hscat=particle.scattered_field.magnetic_field(x,y,z,wl)
    einit = np.array(einit)
    hinit = np.array(hinit)
    hscat = np.array(hscat)
    escat = np.array(escat)
    return einit,escat,hinit,hscat

def test_decomposition(einit,escat,hinit,hscat):
    einit = np.array(einit)
    hinit = np.array(hinit)
    hscat = np.array(hscat)
    escat = np.array(escat)
    e=einit+escat
    h=hinit+hscat
    print(escat.shape)
    print(hscat.shape)
    chirtot=np.sum(np.conj(e)*h,axis=0)
    chirtot2=np.sum(np.conj(escat)*hscat,axis=0)+np.sum(np.conj(einit)*hscat,axis=0)++np.sum(np.conj(escat)*hinit,axis=0)+np.sum(np.conj(einit)*hinit,axis=0)
    #chirtot2=np.sum(np.conj(einit)*(hinit+hscat)+np.conj(escat)*(hinit+hscat),axis=0)
    return np.mean(np.abs(chirtot-chirtot2))/np.mean(np.abs(chirtot))

def average_chirality(particle,wl,r,pw,ls):
    nsurr=ls.refractive_indices[0]
    coef=particle.scattered_field.coefficients
    coefi=pw.spherical_wave_expansion(particle,ls).coefficients
    total=0.
    totalis=0.
    totalsi=0.
    l_max=particle.l_max
    kr=2*np.pi*r/wl*nsurr
    for l in range(1,l_max+1):    
        bes = sf.spherical_hankel(l, kr)
        dxxz = sf.dx_xh(l, kr)
        bes_kr = bes / kr
        dxxz_kr = dxxz / kr

        bes1 = sf.spherical_bessel(l, kr)
        dxxz1 = sf.dx_xj(l, kr)
        bes1_kr = bes1 / kr
        dxxz1_kr = dxxz1 / kr

        tau=1
        m=1
        b_el=coef[multi_to_single_index(tau, l, m, l_max, l_max)]
        b_mag=coef[multi_to_single_index(1-tau, l, m, l_max, l_max)]
        a_el=coefi[multi_to_single_index(tau, l, m, l_max, l_max)]
        a_mag=coefi[multi_to_single_index(1-tau, l, m, l_max, l_max)]
        p1=np.conj(b_mag)*b_el*np.pi*np.abs(bes)**2
        p2=np.conj(b_el)*b_mag*np.pi*(np.abs(dxxz_kr)**2+l*(l+1)*np.abs(bes_kr)**2)

        p1i=np.pi*np.conj(bes1)*bes*np.conj(a_mag)*b_el
        p2i=np.pi*(np.conj(dxxz1_kr)*dxxz_kr+l*(l+1)*np.conj(bes1_kr)*bes_kr)*np.conj(a_el)*b_mag

        p1s=np.pi*np.conj(bes)*bes1*np.conj(b_mag)*a_el
        p2s=np.pi*(np.conj(dxxz_kr)*dxxz1_kr+l*(l+1)*np.conj(bes_kr)*bes1_kr)*np.conj(b_el)*a_mag
        partial_chirality=(p1+p2)*(-1j)
        pis=(p1i+p2i)*(-1j)
        psi=(p1s+p2s)*(-1j)
        total+=partial_chirality
        totalis+=pis
        totalsi+=psi
    totalmax=total+totalis+totalsi    
    return 1-totalmax.imag/(4*np.pi)/nsurr

if __name__ == '__main__':
    from time import time
    rad=200
    he=200
    wl=1500
    l_max=2
    ns=1.
    ls=LayerSystem(refractive_indices=[ns,ns])
    particle=FiniteCylinder(cylinder_radius=rad,cylinder_height=he,l_max=l_max,n_rank=8,position=[0,0,3*rad],refractive_index=4)
    pw=PlaneWave(wl,0,0,'L',reference_point=particle.position)
    sim=Simulation(particle_list=[particle],layer_system=ls,initial_field=pw)
    sim.run()

    r=rad+100

    st=time()
    chirint=average_chirality(particle,wl,r,pw,ls)
    print('elapsed:',time()-st,' result:',chirint)
    st=time()
    totalmax,totalmax2=calculate_chirality_int2(particle,wl,r,ns)    
    print('elapsed:',time()-st,' result:',totalmax,totalmax2)

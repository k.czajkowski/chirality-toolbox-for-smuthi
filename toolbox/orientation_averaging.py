from smuthi.fields import blocksize
import numpy as np
from smuthi.particles import FiniteCylinder
from smuthi.layers import LayerSystem
from smuthi.initial_field import PlaneWave
from smuthi.fields import multi_to_single_index
from smuthi.simulation import Simulation
import smuthi.utility.math as sf
import scipy.io as sio
import matplotlib.pyplot as plt
#from av_fun import average_chirality

def flip_t_matrix(Ttst,lmax):
    bs=blocksize(lmax,lmax)//2
    Tmm=Ttst[:bs,:bs]
    Tme=Ttst[:bs,bs:]
    Tem=Ttst[bs:,:bs]
    Tee=Ttst[bs:,bs:]

    Tn=np.block([[Tem,Tee],[Tmm,Tme]])    
    return Tn

def average_matrix(Ttst,lmax):
    bs=blocksize(lmax,lmax)//2
    Tmm=Ttst[:bs,:bs]
    Tme=Ttst[:bs,bs:]
    Tem=Ttst[bs:,:bs]
    Tee=Ttst[bs:,bs:]

    Tnmm=0*Tmm
    Tnme=0*Tme
    Tnem=Tem*0
    Tnee=0*Tee

    ii=0
    for l in range(1,lmax+1):
        Tnmm[ii:ii+2*l+1,ii:ii+2*l+1]=Tmm[ii:ii+2*l+1,ii:ii+2*l+1]*0+np.sum(np.diag(Tmm[ii:ii+2*l+1,ii:ii+2*l+1]))/(2*l+1)
        Tnme[ii:ii+2*l+1,ii:ii+2*l+1]=Tme[ii:ii+2*l+1,ii:ii+2*l+1]*0+np.sum(np.diag(Tme[ii:ii+2*l+1,ii:ii+2*l+1]))/(2*l+1)
        Tnem[ii:ii+2*l+1,ii:ii+2*l+1]=Tem[ii:ii+2*l+1,ii:ii+2*l+1]*0+np.sum(np.diag(Tem[ii:ii+2*l+1,ii:ii+2*l+1]))/(2*l+1)
        Tnee[ii:ii+2*l+1,ii:ii+2*l+1]=Tee[ii:ii+2*l+1,ii:ii+2*l+1]*0+np.sum(np.diag(Tee[ii:ii+2*l+1,ii:ii+2*l+1]))/(2*l+1)
        ii+=2*l+1
    Tn=np.block([[Tnmm,Tnme],[Tnem,Tnee]])    
    return Tn

def average_chirality_new(particle,wl,r,pw,ls,average=False):
    nsurr=ls.refractive_indices[0]
    T=particle.compute_t_matrix(wl,nsurr)
    flipT=flip_t_matrix(T,particle.l_max)
    coefi=pw.spherical_wave_expansion(particle,ls).coefficients
    total=0.
    totalis=0.
    totalsi=0.
    l_max=particle.l_max
    kr=2*np.pi*r/wl*nsurr
    fac=0j*T  
    face1=0j*T  
    face2=0j*T 
    for l in range(1,l_max+1):    
        bes = sf.spherical_hankel(l, kr)
        dxxz = sf.dx_xh(l, kr)
        bes_kr = bes / kr
        dxxz_kr = dxxz / kr

        bes1 = sf.spherical_bessel(l, kr)
        dxxz1 = sf.dx_xj(l, kr)
        bes1_kr = bes1 / kr
        dxxz1_kr = dxxz1 / kr
        tau=1
        for m in range(-l,l+1):
            ind=multi_to_single_index(tau, l, m, l_max, l_max)
            fac[ind,ind]=np.pi*np.abs(bes)**2
            face1[ind,ind]=np.pi*np.conj(bes1)*bes
            face2[ind,ind]=np.pi*(np.conj(dxxz_kr)*dxxz1_kr+l*(l+1)*np.conj(bes_kr)*bes1_kr)
            ind=multi_to_single_index(1-tau, l, m, l_max, l_max)
            fac[ind,ind]=np.pi*(np.abs(dxxz_kr)**2+l*(l+1)*np.abs(bes_kr)**2)
            face1[ind,ind]=np.pi*(np.conj(dxxz1_kr)*dxxz_kr+l*(l+1)*np.conj(bes1_kr)*bes_kr)
            face2[ind,ind]=np.pi*np.conj(bes)*bes1
        
    W=flipT.conj().T@fac@T
    if average:
        W=average_matrix(W,l_max)
        T=average_matrix(T,l_max)
    pscat=coefi.conj()@W@coefi*(-1j) #fac@ 
    pis=coefi.conj()@face1@T@coefi*(-1j)
    pis1=(coefi.conj()@face2.conj()@T@coefi).conj()*(-1j)
    totalmax=pis+pis1+pscat
    return 1-totalmax.imag/(4*np.pi)

def calculate_chirality_new(particle,wl,r,nsurr):    
    ls=LayerSystem(refractive_indices=[nsurr,nsurr])
    pw=PlaneWave(wl,0,0,'L',reference_point=particle.position)
    sim=Simulation(particle_list=[particle],layer_system=ls,initial_field=pw)
    sim.run()
    chirality=average_chirality_new(particle,wl,r,pw,ls)
    return chirality

if __name__=='__main__':
    from smuthi.particles import FiniteCylinder,Sphere
    from smuthi.layers import LayerSystem
    from smuthi.initial_field import PlaneWave
    from smuthi.simulation import Simulation
    import matplotlib.pyplot as plt
    particle=FiniteCylinder(cylinder_radius=80,cylinder_height=120,refractive_index=4,l_max=3)
    #particle=Sphere(radius=100,refractive_index=4,l_max=3)
    r=200
    ls=LayerSystem()
    wlvec=np.linspace(300,1000,51)
    wyn=0j*wlvec
    wyn2=0j*wlvec
    for iwl,wl in enumerate(wlvec):
      pw=PlaneWave(wl,0,0,'L')
      sim=Simulation(particle_list=[particle],layer_system=ls,initial_field=pw)
      sim.run()
      wyn[iwl]=average_chirality(particle,wl,r,pw,ls)
      wyn2[iwl]=average_chirality_new(particle,wl,r,pw,ls,True)

    plt.figure()
    plt.plot(wlvec,wyn.real)
    plt.plot(wlvec,wyn2.real,'--')
plt.show()

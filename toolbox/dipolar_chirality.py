import numpy as np

def chirality_average(tmat,lam,r,nsurr):
    k=2*np.pi/lam*nsurr
    alfc = -3j/(2*k**3)
    alfe0=tmat[3,3]
    alfm0=tmat[0,0]

    alfm = alfc*alfe0
    alfe = alfc*alfm0

    alfmi=alfm.imag
    alfmr=alfm.real
    alfer=alfe.real
    alfei=alfe.imag

    alfp=1/np.sqrt(2)*(alfm+alfe)
    alfmi=1/np.sqrt(2)*(alfe-alfm)
    delta=np.abs(alfp)**2-np.abs(alfmi)**2
    w = (2*k**4*r**4+2*k**2*r**2+3)/(3*r**6)
    wb = -w*(3*np.imag(alfp))/(np.sqrt(2)*k**3)
    wc = np.real(alfp*(np.exp(2*1j*k*r)*(-6*k*r - 1j*(3-4*k**2*r**2)))/(np.sqrt(2)*k**3*r**6)) 
    f=1+w*delta+wb+wc
    return f


def chirality_x(tmat,lam,r,nsurr):
    k=2*np.pi/lam*nsurr
    alfc = -3j/(2*k**3)
    alfe0=tmat[3,3]
    alfm0=tmat[0,0]

    alfm = alfc*alfe0
    alfe = alfc*alfm0

    alfmi=alfm.imag
    alfmr=alfm.real
    alfer=alfe.real    
    alfei=alfe.imag

    alfp=1/np.sqrt(2)*(alfm+alfe)
    alfmi=1/np.sqrt(2)*(alfe-alfm)
    delta=np.abs(alfp)**2-np.abs(alfmi)**2
    #print('alfmr:',alfmr,1/np.sqrt(2)*np.real(alfp - alfmi))
    wb=np.real(alfp*(np.exp(1j*k*r)*(k**2*r**2+1 -1j*k*r))/(np.sqrt(2)*r**3))
    f=1+delta*(2*k**4*r**4+4*k**2*r**2+5)/(4*r**6)+wb
    return f

def chirality_z(tmat,lam,r,nsurr):
    k=2*np.pi/lam*nsurr
    alfc = -3j/(2*k**3)
    alfe0=tmat[3,3]
    alfm0=tmat[0,0]

    alfm = alfc*alfe0
    alfe = alfc*alfm0

    alfmi=alfm.imag
    alfmr=alfm.real
    alfer=alfe.real
    alfei=alfe.imag

    alfp=1/np.sqrt(2)*(alfm+alfe)
    alfmi=1/np.sqrt(2)*(alfe-alfm)
    delta=np.abs(alfp)**2-np.abs(alfmi)**2
    wb=np.real(alfp*(np.sqrt(2)*(2*k**2*r**2-1)+1j*2*np.sqrt(2)*k*r)/(r**3))
    wc=np.abs(alfp)**2*(4*k**4*r**4)/(2*r**6)
    f=1+delta/(2*r**6)+wb+wc
    return f

if __name__=='__main__':
    from smuthi.particles import Sphere
    par=Sphere(refractive_index=4,radius=100,l_max=1,position=[0,0,0])
    r,nsurr=(150,1)
    lam=600
    tmat=par.compute_t_matrix(lam,nsurr)
    xyben=dipolar_chirality_xy(tmat,lam,r,nsurr)
    xy=xnew(tmat,lam,r,nsurr)
    print(xyben,xy)
    zben=dipolar_chirality_z2(tmat,lam,r,nsurr)
    z=znew(tmat,lam,r,nsurr)
    print(zben,z)
    avgben=dipolar_chirality2(tmat,lam,r,nsurr)
    avg=avgnew(tmat,lam,r,nsurr)
    print(avgben,avg)

from smuthi.particles import FiniteCylinder
from smuthi.layers import LayerSystem
from initial_field import PlaneWave
from smuthi.simulation import Simulation

import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt


epsd,epss=(11.9,2.25)
ri=np.sqrt(epsd)#+0.2j
nmax=3
rad,he = 448,400

thz = 299792.45800
#layers
nsurr=np.sqrt(epss)
nsub=np.sqrt(epss)

#chiral
rich=np.sqrt(2+0.1j)
radch=0.25
chi=0.1

#position
dist=200
pch=[radch+rad+dist,0,0]
    
#lamvec=np.linspace(400,1000,121)
#lamvec=np.linspace(400,1000,121)
freqvec=np.linspace(80,160,81)
lamvec = thz/freqvec  
etot_pol=[]
htot_pol=[]
for pol in ['L','R']:    
    etot=[]
    htot=[]
    for il,lam in enumerate(lamvec):
        particle = FiniteCylinder(refractive_index=ri,position=[0,0,0],l_max=4,cylinder_radius=rad,cylinder_height=he,n_rank=nmax+5)    
        par=[particle]
        ls=LayerSystem(refractive_indices=[nsurr,nsub])  
        source=PlaneWave(lam,0,0,pol)
        
        einit = np.array(source.electric_field(*pch,layer_system=ls))
        hinit = np.array(source.magnetic_field(*pch,layer_system=ls))
        
        sim=Simulation(initial_field=source,layer_system=ls,particle_list=par)
        sim.run()
        escat = np.array(par[0].scattered_field.electric_field(*pch))
        hscat = np.array(par[0].scattered_field.magnetic_field(*pch,lam))
        etot.append(einit+escat)
        htot.append(hinit+hscat)
    etot_pol.append(etot)
    htot_pol.append(htot)
etot = np.array(etot_pol)
htot = np.array(htot_pol)
sio.savemat('explicit_chirality3_lmax4.mat',{'wl':lamvec,'etot':etot,'htot':htot})
